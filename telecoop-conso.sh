#!/bin/bash
# Simple script to get consumptions from Telecoop's website

[[ -z "${ACCOUNT_EMAIL}" ]] && ACCOUNT_EMAIL="REPLACE_ME"
[[ -z "${ACCOUNT_PASS}" ]] && ACCOUNT_PASS="REPLACE_ME"
[[ -z "${ACCOUNT_PHONE}" ]] && ACCOUNT_PHONE=""            # Optional, can be set by command line


function usage() {
    echo "Usage:"
    echo "      $0 [-h] [-q] [-d] [-s] [-v] [-p 06XXXXXXXX]"
    echo ""
    echo "With:"
    echo "  -h: Print this helpful message"
    echo "  -q: Quiet (show values only, without label"
    echo "  -d: Show DATA consumption for the current month"
    echo "  -s: Show SMS consumption for the current month"
    echo "  -v: Show VOICE consumption for the current month"
    echo "  -p: Show consumption for this phone line"
}


# Default options
VOICE=0
DATA=0
SMS=0
QUIET=0

while getopts hvdsqp: options; do
    case $options in
        h)
            usage
            exit 0
            ;;
        v) VOICE=1 ;;
        d) DATA=1 ;;
        s) SMS=1 ;;
        q) QUIET=1 ;;
        p) ACCOUNT_PHONE=$OPTARG;;
    esac
done

TMP_COOKIE="$( mktemp )"
API_WEBSITE="https://espace-personnel-api.telecoop.fr"
API_PATH="api/customer"
CURRENT_MONTH=$( date +%m | sed -r 's/^0+//g')
CURRENT_YEAR=$( date +%Y )

if [ "${ACCOUNT_EMAIL}" = "REPLACE_ME" -o "${ACCOUNT_PASS}" = "REPLACE_ME" ] ; then
    echo "Please edit this script and customize ACCOUNT_EMAIL and ACCOUNT_PASS with"
    echo "the credentials used to login on https://espace-personnel.telecoop.fr/login"
    exit 1
fi

if [ -z "${ACCOUNT_PHONE}" ] ; then
    echo "Please provide phone line:"
    echo "- either by providing the '-p [phoneline]' parameter"
    echo "- or by customizing ACCOUNT_PHONE direcly in script"
    usage
    exit 1
fi

# Login to espace client
LOGIN=$( curl -c "${TMP_COOKIE}" --silent -X POST \
    -d "email=${ACCOUNT_EMAIL}&password=${ACCOUNT_PASS}" \
    ${API_WEBSITE}/${API_PATH}/login )
TOKEN=$( echo "${LOGIN}" | jq -r .token 2>/dev/null )
TOKEN_TYPE=$( echo "${LOGIN}" | jq -r .token_type 2>/dev/null )
if [ -z "${TOKEN}" ]; then
    echo "Error: Credentials seem to be unauthorized."
    exit 1
fi

# Get consumption data
CONSUMPTION=$( curl -b "${TMP_COOKIE}" --silent \
                -H "Authorization: ${TOKEN_TYPE} ${TOKEN}" \
    ${API_WEBSITE}/${API_PATH}/consumption/last-twelve-months/${CURRENT_YEAR}/${CURRENT_MONTH}?line=${ACCOUNT_PHONE} )


LAST_CONSUMPTION=$( echo "${CONSUMPTION}" | jq '.data[-1]' )
if [ "$VOICE" -eq "1" ]; then
    [[ "${QUIET}" -eq "0" ]] && echo -n "Voice: "
    echo "${LAST_CONSUMPTION}" | jq .voice.value
fi
if [ "$SMS" -eq "1" ]; then
    [[ "${QUIET}" -eq "0" ]] && echo -n "SMS: "
    echo "${LAST_CONSUMPTION}" | jq .sms.value
fi
if [ "$DATA" -eq "1" ]; then
    [[ "${QUIET}" -eq "0" ]] && echo -n "Data: "
    echo "${LAST_CONSUMPTION}" | jq .data.value
fi

[[ -f "${TMP_COOKIE}" ]] && rm -f "${TMP_COOKIE}"
