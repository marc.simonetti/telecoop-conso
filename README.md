# Telecoop Conso

Simple script to get personal consumption.

## Prerequisites:

* Edit the script and customize the `REPLACE_ME` with your personal information, the one used to login [here](https://espace-personnel.telecoop.fr/login).
* `jq` already installed and available

## Usage

```
$ telecoop_conso.sh [-h] [-q] [-d] [-s] [-v] [-p 06XXXXXXXX]
```

With:
* h: Print Help message
* q: Quiet (show values only, without label
* d: Show DATA consumption for the current month
* s: Show SMS consumption for the current month
* v: Show VOICE consumption for the current month
* p [phonenumber]: Show consumption for this phone line

## Example:

```
$ ./telecoop_conso.sh -d -p 0601020304
Data: 1.08
```
